We think that one of our strong points is our program. In the last season we had a hard time deciding what kind of robot to use and what kind of attachments to build. At first, each time we'd change these things we had also to build a fresh new program so our need to have some universal blocs had rised. So, we worked toghether with another team from Brasov, Robojunii, to build custom blocs that would help us with the First Lego Legue competition. 
Our main blocs are: 
    -straightWalk wich can make the robot to go in a straight line (using the gyro sensor) a certain number of centimeters;
    -spin used to rotate the robot until the gyro reach a certain angle
    -line is a bloc simmilar to straightWalk but it will go straight until the robot reaches a black/white line
    -stall will make one of the motors to rotate until the motor is stalled(useful when the attachments have mechanical limits)
    -setPorts used to set the ports for the two motors that move the robot and the gyro sensor (these are used in all the custom blocs)

All these blocs started to make the program more and more complex and to further simplify the program we also builded some blocs used solely in the main ones such as:
    -verifyPort check if the input port is in range 1-4
    -accel used to rise or decrease the speed of the robot gradually. It also keep track of the distance the robot had made during the process(usefull when used in straightWalk)
    -stop is used to slow down the speed of the 2 motors that move the robot, it operates separate on the two motors(so, the motors can have different initial speeds)
    -openLoop is used to control the speed of the motors in such a way that the robot will keep walking straight, for that the bloc use the gyro sensor and the proportional control algorithm (where you calculate the error, in our case how far we are from walking straight, and use that to adjust the power of the motors). The openLoop bloc must be used in a loop to update the power of the motors continuously.
    -break used to force stop the robot when the input condition is true.

After we have all these custom blocs, the process of making a new program is simplified, now one have to merly put them in the correct order. 
